/******************************************************************************************\
*                                                                                          *
*   I2CSniffer.cpp                                                                         *
*                                                                                          *
*       Tool to sniff I2C communication on system SMBus interfaces on Windows              *
*                                                                                          *
\******************************************************************************************/

#include "i2c_smbus.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include <tchar.h>
#include <regex>
#include "i2c_smbus_piix4.h"
#include "i2c_smbus_i801.h"
#include "i2c_smbus_nuvoton_nct6793d.h"
#include "wmi.h"
#include "inpout32.h"

#pragma comment(lib, "inpout32.lib")

std::vector<i2c_smbus_interface *> busses;

/******************************************************************************************\
*                                                                                          *
*   Nuvoton Super IO constants                                                             *
*                                                                                          *
\******************************************************************************************/

#define SIO_NCT5577_ID      0xC330  /* Device ID for NCT5577D (C333)        */
#define SIO_NCT6102_ID      0x1060  /* Device ID for NCT6102D/6106D (1061)  */
#define SIO_NCT6793_ID      0xd120  /* Device ID for NCT6793D (D121)        */
#define SIO_NCT6796_ID      0xd420  /* Device ID for NCT6796D (D421)        */
#define SIO_REG_LOGDEV      0x07    /* Logical Device Register              */
#define SIO_REG_DEVID       0x20    /* Device ID Register                   */
#define SIO_REG_SMBA        0x62    /* SMBus Base Address Register          */
#define SIO_LOGDEV_SMBUS    0x0B    /* Logical Device for SMBus             */
#define SIO_ID_MASK         0xFFF8  /* Device ID mask                       */


/******************************************************************************************\
*                                                                                          *
*   superio_enter                                                                          *
*                                                                                          *
*   Put the Super IO chip into Extended Function Mode                                      *
*                                                                                          *
\******************************************************************************************/

void superio_enter(int ioreg)
{
    Out32(ioreg, 0x87);
    Out32(ioreg, 0x87);
}


/******************************************************************************************\
*                                                                                          *
*   superio_outb                                                                           *
*                                                                                          *
*   Write a byte to the Super IO configuration register                                    *
*                                                                                          *
\******************************************************************************************/

void superio_outb(int ioreg, int reg, int val)
{
    Out32(ioreg, reg);
    Out32(ioreg + 1, val);
}


/******************************************************************************************\
*                                                                                          *
*   superio_inb                                                                            *
*                                                                                          *
*   Read a byte to the Super IO configuration register                                     *
*                                                                                          *
\******************************************************************************************/

int superio_inb(int ioreg, int reg)
{
    Out32(ioreg, reg);
    return Inp32(ioreg + 1);
}


/******************************************************************************************\
*                                                                                          *
*   DetectNuvotonI2CBusses (Windows)                                                       *
*                                                                                          *
*       Detects available Nuvoton Super IO SMBUS adapters and enumerates                   *
*       i2c_smbus_interface objects for them                                               *
*                                                                                          *
\******************************************************************************************/

void DetectNuvotonI2CBusses()
{
    i2c_smbus_interface* bus;
    int sioaddr = 0x2E;
    superio_enter(sioaddr);

    int val = (superio_inb(sioaddr, SIO_REG_DEVID) << 8) | superio_inb(sioaddr, SIO_REG_DEVID + 1);

    switch (val & SIO_ID_MASK)
    {
    case SIO_NCT5577_ID:
    case SIO_NCT6102_ID:
    case SIO_NCT6793_ID:
    case SIO_NCT6796_ID:
        bus = new i2c_smbus_nuvoton_nct6793d();

        // Set logical device register to get SMBus base address
        superio_outb(sioaddr, SIO_REG_LOGDEV, SIO_LOGDEV_SMBUS);

        // Get SMBus base address from configuration register
        int smba = (superio_inb(sioaddr, SIO_REG_SMBA) << 8) | superio_inb(sioaddr, SIO_REG_SMBA + 1);
        ((i2c_smbus_nuvoton_nct6793d*)bus)->nuvoton_nct6793d_smba = smba;

        // Set device name string
        switch (val & SIO_ID_MASK)
        {
        case SIO_NCT5577_ID:
            sprintf(bus->device_name, "Nuvoton NCT5577D SMBus at %X", smba);
            break;
        case SIO_NCT6102_ID:
            sprintf(bus->device_name, "Nuvoton NCT6102D/NCT6106D SMBus at %X", smba);
            break;
        case SIO_NCT6793_ID:
            sprintf(bus->device_name, "Nuvoton NCT6793D SMBus at %X", smba);
            break;
        case SIO_NCT6796_ID:
            sprintf(bus->device_name, "Nuvoton NCT6796D SMBus at %X", smba);
            break;
        }

        busses.push_back(bus);
    }

}   /* DetectNuvotonI2CBusses() */


/******************************************************************************************\
*                                                                                          *
*   DetectI2CBusses (Windows)                                                              *
*                                                                                          *
*       Detects available AMD and Intel SMBUS adapters and enumerates i2c_smbus_interface  *
*       objects for them                                                                   *
*                                                                                          *
\******************************************************************************************/

void DetectI2CBusses()
{
    i2c_smbus_interface * bus;
    HRESULT hres;
    Wmi wmi;
    wmi.init();

    // Query WMI for Win32_PnPSignedDriver entries with names matching "SMBUS" or "SM BUS"
    // These devices may be browsed under Device Manager -> System Devices
    std::vector<QueryObj> q_res_PnPSignedDriver;
    hres = wmi.query("SELECT * FROM Win32_PnPSignedDriver WHERE Description LIKE '\%SMBUS\%' OR Description LIKE '\%SM BUS\%'", q_res_PnPSignedDriver);

    if (hres)
    {
        return;
    }

    // For each detected SMBus adapter, try enumerating it as either AMD or Intel
    for (QueryObj &i : q_res_PnPSignedDriver)
    {
        // AMD SMBus controllers do not show any I/O resources allocated in Device Manager
        // Analysis of many AMD boards has shown that AMD SMBus controllers have two adapters with fixed I/O spaces at 0x0B00 and 0x0B20
        // AMD SMBus adapters use the PIIX4 driver
        if (i["Manufacturer"].find("Advanced Micro Devices, Inc") != std::string::npos)
        {
            bus = new i2c_smbus_piix4();
            strcpy(bus->device_name, i["Description"].c_str());
            strcat(bus->device_name, " at 0x0B00");
            ((i2c_smbus_piix4 *)bus)->piix4_smba = 0x0B00;
            busses.push_back(bus);

            bus = new i2c_smbus_piix4();
            ((i2c_smbus_piix4 *)bus)->piix4_smba = 0x0B20;
            strcpy(bus->device_name, i["Description"].c_str());
            strcat(bus->device_name, " at 0x0B20");
            busses.push_back(bus);
        }

        // Intel SMBus controllers do show I/O resources in Device Manager
        // Analysis of many Intel boards has shown that Intel SMBus adapter I/O space varies between boards
        // We can query Win32_PnPAllocatedResource entries and look up the PCI device ID to find the allocated I/O space
        // Intel SMBus adapters use the i801 driver
        else if ((i["Manufacturer"].find("Intel") != std::string::npos)
              || (i["Manufacturer"].find("INTEL") != std::string::npos))
        {
            std::string rgx1 = ".+" + q_res_PnPSignedDriver[0]["DeviceID"].substr(4, 33) + ".+";

            AdditionalFilters filters;
            filters.emplace("Dependent", rgx1);
            filters.emplace("Antecedent", ".*Port.*");

            std::vector<QueryObj> q_res_PNPAllocatedResource;
            hres = wmi.query("SELECT * FROM Win32_PnPAllocatedResource", q_res_PNPAllocatedResource, &filters);

            std::regex rgx2(".*StartingAddress=\"(\\d+)\".*");
            std::smatch matches;

            // Query the StartingAddress for the matching device ID and use it to enumerate the bus
            if (std::regex_search(q_res_PNPAllocatedResource[0]["Antecedent"], matches, rgx2))
            {
                unsigned int IORangeStart = std::stoi(matches[1].str());

                bus = new i2c_smbus_i801();
                strcpy(bus->device_name, i["Description"].c_str());
                ((i2c_smbus_i801 *)bus)->i801_smba = IORangeStart;
                busses.push_back(bus);
            }
        }
    }

    // Detect Nuvoton Super IO SMBus adapters
    DetectNuvotonI2CBusses();

}   /* DetectI2CBusses() */

void SniffI2CI801(i2c_smbus_interface* bus)
{
    /* I801 SMBus address offsets */
    #define SMBHSTSTS       (0 + i801_smba)
    #define SMBHSTCNT       (2 + i801_smba)
    #define SMBHSTCMD       (3 + i801_smba)
    #define SMBHSTADD       (4 + i801_smba)
    #define SMBHSTDAT0      (5 + i801_smba)
    #define SMBHSTDAT1      (6 + i801_smba)
    #define SMBBLKDAT       (7 + i801_smba)
    #define SMBPEC          (8 + i801_smba)     /* ICH3 and later */
    #define SMBAUXSTS       (12 + i801_smba)    /* ICH4 and later */
    #define SMBAUXCTL       (13 + i801_smba)    /* ICH4 and later */
    #define SMBSLVSTS       (16 + i801_smba)    /* ICH3 and later */
    #define SMBSLVCMD       (17 + i801_smba)    /* ICH3 and later */
    #define SMBNTFDADD      (20 + i801_smba)    /* ICH3 and later */

    int i801_smba = ((i2c_smbus_i801*)bus)->i801_smba;

    while (1)
    {
        while ((Inp32(SMBHSTSTS) & 0x02) != 0)
        {
            for (int i = 0; i < 5000; i++);
        }

        while ((Inp32(SMBHSTSTS) & 0x02) == 0)
        {
            for (int i = 0; i < 5000; i++);
        }
        {
            unsigned char addr = Inp32(SMBHSTADD);
            unsigned char size = Inp32(SMBHSTCNT);
            unsigned char cmnd = Inp32(SMBHSTCMD);
            unsigned char dat0 = Inp32(SMBHSTDAT0);
            unsigned char dat1 = Inp32(SMBHSTDAT1);

            if (addr & 1)
            {
                switch (size & 0x1C)
                {
                case I801_QUICK:
                    printf("Read %02x from address %02x \n",
                        dat0,
                        addr >> 1);
                    break;

                case I801_BYTE_DATA:
                    printf("Read %02x from %02x, address %02x \n",
                        dat0,
                        cmnd,
                        addr >> 1);
                    break;

                case I801_WORD_DATA:
                    printf("Read %02x %02x from %02x, address %02x \n",
                        dat0,
                        dat1,
                        cmnd,
                        addr >> 1);
                    break;

                case I801_BLOCK_DATA:
                    printf("Read block of length %02x from %02x, address %02x, contents: ",
                        dat0,
                        cmnd,
                        addr >> 1);

                    // Read SMBHSTCNT to reset SMBBLKDAT read pointer
                    Inp32(SMBHSTCNT);

                    for (int byte = 0; byte < dat0; byte++)
                    {
                        printf("%02x ", Inp32(SMBBLKDAT));
                    }

                    printf("\n");

                    break;
                }
            }
            else
            {
                switch (size & 0x1C)
                {
                case I801_QUICK:
                    printf("Wrote %02x to address %02x \n",
                        dat0,
                        addr >> 1);
                    break;

                case I801_BYTE_DATA:
                    printf("Wrote %02x to %02x, address %02x \n",
                        dat0,
                        cmnd,
                        addr >> 1);
                    break;

                case I801_WORD_DATA:
                    printf("Wrote %02x %02x to %02x, address %02x \n",
                        dat0,
                        dat1,
                        cmnd,
                        addr >> 1);
                    break;

                case I801_BLOCK_DATA:
                    printf("Wrote block of length %02x to %02x, address %02x, contents: ",
                        dat0,
                        cmnd,
                        addr >> 1);

                    // Read SMBHSTCNT to reset SMBBLKDAT read pointer
                    Inp32(SMBHSTCNT);

                    for (int byte = 0; byte < dat0; byte++)
                    {
                        printf("%02x ", Inp32(SMBBLKDAT));
                    }

                    printf("\n");

                    break;
                }
            }
        }
    }
}

void SniffI2CPIIX4(i2c_smbus_interface* bus)
{
    // PIIX4 SMBus address offsets
    #define SMBHSTSTS (0 + piix4_smba)
    #define SMBHSLVSTS (1 + piix4_smba)
    #define SMBHSTCNT (2 + piix4_smba)
    #define SMBHSTCMD (3 + piix4_smba)
    #define SMBHSTADD (4 + piix4_smba)
    #define SMBHSTDAT0 (5 + piix4_smba)
    #define SMBHSTDAT1 (6 + piix4_smba)
    #define SMBBLKDAT (7 + piix4_smba)
    #define SMBSLVCNT (8 + piix4_smba)
    #define SMBSHDWCMD (9 + piix4_smba)
    #define SMBSLVEVT (0xA + piix4_smba)
    #define SMBSLVDAT (0xC + piix4_smba)

    int piix4_smba = ((i2c_smbus_piix4*)bus)->piix4_smba;

    while (1)
    {
        while ((Inp32(SMBHSTSTS) & 0x02) != 0)
        {
            for (int i = 0; i < 5000; i++);
        }

        while ((Inp32(SMBHSTSTS) & 0x02) == 0)
        {
            for (int i = 0; i < 5000; i++);
        }
        {
            unsigned char addr = Inp32(SMBHSTADD);
            unsigned char size = Inp32(SMBHSTCNT);
            unsigned char cmnd = Inp32(SMBHSTCMD);
            unsigned char dat0 = Inp32(SMBHSTDAT0);
            unsigned char dat1 = Inp32(SMBHSTDAT1);
            if (addr & 1)
            {
                switch (size & 0x1C)
                {
                case PIIX4_QUICK:
                    printf("Read %02x from address %02x \n",
                        dat0,
                        addr >> 1);
                    break;

                case PIIX4_BYTE_DATA:
                    printf("Read %02x from %02x, address %02x \n",
                        dat0,
                        cmnd,
                        addr >> 1);
                    break;

                case PIIX4_WORD_DATA:
                    printf("Read %02x %02x from %02x, address %02x \n",
                        dat0,
                        dat1,
                        cmnd,
                        addr >> 1);
                    break;

                case PIIX4_BLOCK_DATA:
                    printf("Read block of length %02x from %02x, address %02x, contents: ",
                        dat0,
                        cmnd,
                        addr >> 1);

                    // Read SMBHSTCNT to reset SMBBLKDAT read pointer
                    Inp32(SMBHSTCNT);

                    for (int byte = 0; byte < dat0; byte++)
                    {
                        printf("%02x ", Inp32(SMBBLKDAT));
                    }

                    printf("\n");

                    break;
                }
            }
            else
            {
                switch (size & 0x1C)
                {
                case PIIX4_QUICK:
                    printf("Wrote %02x to address %02x \n",
                        dat0,
                        addr >> 1);
                    break;

                case PIIX4_BYTE_DATA:
                    printf("Wrote %02x to %02x, address %02x \n",
                        dat0,
                        cmnd,
                        addr >> 1);
                    break;

                case PIIX4_WORD_DATA:
                    printf("Wrote %02x %02x to %02x, address %02x \n",
                        dat0,
                        dat1,
                        cmnd,
                        addr >> 1);
                    break;


                case PIIX4_BLOCK_DATA:
                    printf("Wrote block of length %02x to %02x, address %02x, contents: ",
                        dat0,
                        cmnd,
                        addr >> 1);

                    // Read SMBHSTCNT to reset SMBBLKDAT read pointer
                    Inp32(SMBHSTCNT);

                    for (int byte = 0; byte < dat0; byte++)
                    {
                        printf("%02x ", Inp32(SMBBLKDAT));
                    }

                    printf("\n");

                    break;
                }

            }
        }
    }
}

/******************************************************************************************\
*                                                                                          *
*   main                                                                                   *
*                                                                                          *
*       Main function.  Detects busses and starts sniffing                                 *
*                                                                                          *
\******************************************************************************************/

int main(int argc, char *argv[])
{
    DetectI2CBusses();

    printf("I2C Sniffer\r\n");

    for (int bus = 0; bus < busses.size(); bus++)
    {
        printf("    %2d:  %s\r\n", bus, busses[bus]->device_name);
    }

    printf("\r\n");
    printf("Select which bus to sniff: ");
    int selected_bus = 0;

    scanf("%d", &selected_bus);

    if (strncmp("AMD", busses[selected_bus]->device_name, 3) == 0)
    {
        SniffI2CPIIX4(busses[selected_bus]);
    }
    else
    {
        SniffI2CI801(busses[selected_bus]);
    }

    return 0;
}
