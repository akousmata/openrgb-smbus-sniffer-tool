# OpenRGBSnifferTool

This project is an open-source tool used to monitor and log SMBus transactions for reverse engineering.

### Requirements:
* You will need the **Microsoft Visual 2019 C++ runtime** installed.  You can get it [**-->HERE<--**](https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads)
* Download the latest CI build of the SMBus Sniffer tool [**-->HERE<--**](https://gitlab.com/OpenRGBDevelopers/openrgb-smbus-sniffer-tool/-/jobs/artifacts/master/download?job=build)

### Using the tool:
Extract the zip folder and then use a command window to navigate to the extracted I2CSniffer.exe and run it. You will be prompted to select which SMBus/i2c interface you would like to monitor. Use the program to make changes to the device and watch for changes.

### Supporting Documentation:

If you are new to I2C, SMBus, or reverse engineering and are able to review C++ code, this documentation may help you gain a better understanding of how this tool works.

Much of this tool has been aggregated from various other places including from the linux kernel itself.  The tool uses the [inpout32](https://www.highrez.co.uk/downloads/inpout32/#:~:text=InpOut32%20is%20an%20open%20source,XP%2F2003%20etc.) library to interact directly with the SMBus when specifying an address.  In order to do this, the base address of the SMBus device must first be derived before it can be sniffed.  Given the base address, offsets are used to access various registers.  The code for this tool will:

1. Attempt to identify any SMBus busses that may be present on your hardware.
    - This is accomplished using [Windows Management Instrumentation (WMI)](https://docs.microsoft.com/en-us/windows/win32/wmisdk/about-wmi) queries.
    - It checks whether the busses are Intel, AMD, or Nuvoton.
2. Use the i801 (Intel), piix4 (AMD), or Nuvoton drivers to access the SMBus and perform reads of the bus that are present at the various address offsets.  **The documentation below is pulled from Intel data sheets, while the whole point of a spec is consistency, the results may be different for non-Intel drivers**:
    - `SMBHSTSTS` - Host Status Register - used to ensure that it's safe to sniff the bus.  If the bus is busy processing a command or an interrupt has triggered some other process on the bus, don't try to read from it.
    - `SMBHSTADD` - Host Address Register - This register is transmitted by the SMBus controller host interface in the slave address field of the
SMBus protocol.  This register is used to determine whether there's a READ command (1) or a WRITE command (0) on the bus.
    - `SMBHSTCNT` -  Host Control Register - Bits 4:2 are used to determine the command protocol.  Thus, this register is bitwise anded against `0x1C` (28 decimal, 11100 binary) to determine which of the following commands is being used:
      - 0,0,0 Quick Read or Write
      - 0,0,1 Byte Read or Write
      - 0,1,0 Byte Data Read or Write
      - 0,1,1 Word Data Read or Write
      - 1,0,0 Block Read or Write
      - 1,0,1 Reserved
      - 1,1,0 Reserved
      - 1,1,1 Reserved
    - `SMBHSTCMD` - Host Command Register - when the above check results in byte, word or block data read or writes, this register is output to the console to indicate which command was being used.  
    - `SMBHSTDAT0`/`SMBHSTDAT1`  - Host Data 0 / Host Data 1 registers - The following is from an Intel data sheet related to these registers:
      > SMBus Data 0 (SMBD0)–R/W. This register should be programmed with the value to be
transmitted in the Data0 field of an SMBus host interface transaction. For a block write
command, the count of the memory block should be stored in this field. The value of this register
is loaded into the block transfer count field. This register must be programmed to a value
between 1 and 32 for block command counts. A count of 0 or a count above 32 will result in
unpredictable behavior. For block reads, the count received from the SMBus device is stored
here.
3. Based on all the checks done in step 2, the values of the relevant registers is output into the console to show what the SMBus is doing.

Additional links:

- [kernel.org i2c-801 drivers documentation](https://www.kernel.org/doc/html/latest/i2c/busses/i2c-i801.html)
- [i2c-801 drivers linux source](https://github.com/torvalds/linux/blob/master/drivers/i2c/busses/i2c-i801.c)
- [Intel datasheets](https://www.intel.com/content/www/us/en/products/docs/processors/core/core-technical-resources.html)
- While the datasheets help with some aspects of understanding an SMBus, the specifics of all the registers and how they work was much more difficult to track down.  There is an [old datasheet ](https://www.manualslib.com/products/Intel-460gx-243538.html)(c. 2001) that provides quite a bit of detail which is where much of this documentation was pulled from, but may or may not be accurate.
